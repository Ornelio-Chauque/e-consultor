package com.neodoli.finclient.service;

import com.neodoli.finclient.services.IrpsFacade;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class IrpsFacadeTest {

    @Test
    public void givenRawSalaryAndDependentsThenReturnIrps(){
        IrpsFacade irpsFacade= new IrpsFacade();
        assertThat(irpsFacade.calculate(35000,0)).isEqualTo(2225);
        assertThat(irpsFacade.calculate(19000,4)).isEqualTo(0);
    }
}
