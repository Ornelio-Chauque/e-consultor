package com.neodoli.finclient.service;

import com.neodoli.finclient.domain.DiscountPremise;
import com.neodoli.finclient.services.IrpsGroupCoefficentDiscountConfigure;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class IrpsGroupCoefficentDiscountConfigureTest {
    Logger logger= LoggerFactory.getLogger(IrpsGroupCoefficentDiscountConfigure.class);

    Myclass myclass= mock(Myclass.class);


    @Test
    public void whenBaseSalaryIsLessThan20249_99ThenCoefficientIsEqualZero() {
        IrpsGroupCoefficentDiscountConfigure configure = new IrpsGroupCoefficentDiscountConfigure();
        DiscountPremise premise = new DiscountPremise(20000, 0);

        logger.info("Before: This is mocked string method return: " +myclass.myString() );
        logger.info("Before: This is mocked int method return: " +myclass.myInt());


        when(myclass.myString()).thenReturn("Mocked String");
        doReturn(200).when(myclass).myInt();
        logger.info("After: This is mocked string method return: " +myclass.myString() );
        logger.info("After: This is mocked int method return: " +myclass.myInt());
        assertThat(configure.configure(premise)).isEqualTo(0);

    }

    private class Myclass {
        public String myString() {
            return "string";
        }

        public int myInt() {
            return 10;
        }

    }
}
