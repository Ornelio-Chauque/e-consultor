package com.neodoli.finclient;

import com.neodoli.finclient.mycontroller.HomeController;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@SpringBootTest
@AutoConfigureMockMvc
class FinClientApplicationTests {

	@Autowired
	MockMvc mockMvc;

	@Test
	public void whenRootThenReturnHomeAsViewAndIndexAsMethodHandler() throws Exception{
		MvcResult mvcResult =mockMvc.perform(get("/"))
				.andExpect(view().name("home"))
				.andExpect(handler().methodName("index"))
				.andReturn();
		assertThat("home").isEqualTo(mvcResult.getModelAndView().getViewName());
		assertThat("index()").isEqualTo(mvcResult.getHandler().toString().split("#")[1]);
	}

}
