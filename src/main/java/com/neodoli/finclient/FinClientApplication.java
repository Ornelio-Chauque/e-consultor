package com.neodoli.finclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinClientApplication.class, args);
	}

}
