package com.neodoli.finclient.domain;

public class DiscountPremise {
    double baseSalary;
    int dependentNumber;

    public DiscountPremise(double baseSalary, int dependentNumber){
        this.baseSalary=baseSalary;
        this.dependentNumber=dependentNumber;
    }


    public double getBaseSalary() {
        return baseSalary;
    }

    public int getDependentNumber() {
        return dependentNumber;
    }
}
