package com.neodoli.finclient.domain;

import java.util.Objects;

public class IrpsResult {
    private double rawSalary;
    private double IrpsDiscount;
    private int numberDependents;

    public IrpsResult() {
    }

    ;

    public double getRawSalary() {
        return rawSalary;
    }

    public void setRawSalary(double rawSalary) {
        this.rawSalary = rawSalary;
    }

    public double getIrpsDiscount() {
        return IrpsDiscount;
    }

    public void setIrpsDiscount(double irpsDiscount) {
        IrpsDiscount = irpsDiscount;
    }

    public int getNumberDependents() {
        return numberDependents;
    }

    public void setNumberDependents(int numberDependents) {
        this.numberDependents = numberDependents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IrpsResult that = (IrpsResult) o;
        return Double.compare(that.rawSalary, rawSalary) == 0 && Double.compare(that.IrpsDiscount, IrpsDiscount) == 0 && numberDependents == that.numberDependents;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rawSalary, IrpsDiscount, numberDependents);
    }
}
