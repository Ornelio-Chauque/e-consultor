package com.neodoli.finclient.services;

import javax.validation.constraints.Size;

public class IRPSDiscountResult {
    private float coefficientPercentage;
    private float minimumValueAtSalaryGroup;
    private float discountOverDependents;

    public float getCoefficientPercentage() {
        return coefficientPercentage;
    }

    public void setCoefficientPercentage(float coefficientPercentage) {
        this.coefficientPercentage = coefficientPercentage;
    }

    public float getMinimumValueAtSalaryGroup() {
        return minimumValueAtSalaryGroup;
    }

    public void setMinimumValueAtSalaryGroup(float minimumValueAtSalaryGroup) {
        this.minimumValueAtSalaryGroup = minimumValueAtSalaryGroup;
    }

    public float getDiscountOverDependents() {
        return discountOverDependents;
    }

    public void setDiscountOverDependents(float discountOverDependents) {
        this.discountOverDependents = discountOverDependents;
    }
}
