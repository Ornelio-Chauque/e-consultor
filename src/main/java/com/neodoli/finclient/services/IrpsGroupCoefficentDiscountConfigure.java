package com.neodoli.finclient.services;

import com.neodoli.finclient.domain.DiscountPremise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IrpsGroupCoefficentDiscountConfigure implements IRPSConfigure{
    private static final Logger LOGGER= LoggerFactory.getLogger(IrpsGroupCoefficentDiscountConfigure.class);
    @Override
    public double configure(DiscountPremise premise) {
        double coefficientDiscount;
        if(premise.getBaseSalary() <=20249.99){
            coefficientDiscount=0.0;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if( premise.getBaseSalary()>=20250.00 && premise.getBaseSalary()<=20749.99){
            coefficientDiscount=premise.getDependentNumber()==0?0.10:0.0;;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if(premise.getBaseSalary()>=20750 && premise.getBaseSalary()<=20999.99){
            coefficientDiscount=premise.getDependentNumber()>1?0.0:0.10;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if(premise.getBaseSalary()>=21000 && premise.getBaseSalary()<=21249.99){
            coefficientDiscount=premise.getDependentNumber()>2?0.1:0.0;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if(premise.getBaseSalary()>=21250 && premise.getBaseSalary()<=21749.99){
            coefficientDiscount=premise.getDependentNumber()>3?0.1:0.0;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if(premise.getBaseSalary()>=21750 && premise.getBaseSalary()<=22249.99){
            coefficientDiscount=0.10;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if(premise.getBaseSalary()>=22250 && premise.getBaseSalary()<=32749.99){
            coefficientDiscount=0.15;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if(premise.getBaseSalary()>=32750 && premise.getBaseSalary()<=60749.99){
            coefficientDiscount=0.20;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if(premise.getBaseSalary()>=60750 && premise.getBaseSalary()<=144749.99){
            coefficientDiscount=0.25;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }else if(premise.getBaseSalary()>=144750){
            coefficientDiscount=0.32;
            LOGGER.info("The generated coefficient is "+coefficientDiscount);
            return coefficientDiscount;
        }
        throw new IllegalArgumentException(" The data provided are invalid");

    }
}
