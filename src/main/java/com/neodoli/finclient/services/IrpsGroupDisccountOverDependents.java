package com.neodoli.finclient.services;

import com.neodoli.finclient.domain.DiscountPremise;

public class IrpsGroupDisccountOverDependents implements IRPSConfigure {

    @Override
    public double configure(DiscountPremise premise) {
        double maximumDiscountOverDependentsOnGroup;
        if (premise.getBaseSalary() <= 20749.99) {
            maximumDiscountOverDependentsOnGroup=0.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        else if (premise.getBaseSalary() >= 20750 && premise.getBaseSalary() <= 20999.99) {
            maximumDiscountOverDependentsOnGroup=50.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        else if (premise.getBaseSalary() >= 21000 && premise.getBaseSalary() <= 21249.99) {
            maximumDiscountOverDependentsOnGroup=75.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        else if (premise.getBaseSalary() >= 21250 && premise.getBaseSalary() <= 21749.99) {
            maximumDiscountOverDependentsOnGroup=100.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        else if (premise.getBaseSalary() >= 21750 && premise.getBaseSalary() <= 22249.99) {
            maximumDiscountOverDependentsOnGroup=150.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        else if (premise.getBaseSalary() >= 22250 && premise.getBaseSalary() <= 32749.99) {
            maximumDiscountOverDependentsOnGroup=200.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        else if (premise.getBaseSalary() >= 32750 && premise.getBaseSalary() <= 60749.99) {
            maximumDiscountOverDependentsOnGroup=1775.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        else if (premise.getBaseSalary() >= 60750 && premise.getBaseSalary() <= 144749.99) {
            maximumDiscountOverDependentsOnGroup=7375.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        else if (premise.getBaseSalary() >= 144750) {
            maximumDiscountOverDependentsOnGroup = 28375.0;
            return generateDiscountOverDependents(maximumDiscountOverDependentsOnGroup, premise.getDependentNumber());
        }
        throw new IllegalArgumentException(" The data provided are invalid");

    }

    private double generateDiscountOverDependents(double maximumDiscountOverDependentsOnGroup, int numberDependents) {
        double resultDiscountOverDependents;
        switch (numberDependents) {
            case 0:
                resultDiscountOverDependents = maximumDiscountOverDependentsOnGroup;
                return resultDiscountOverDependents <= 0 ? 0 : resultDiscountOverDependents;

            case 1:
                resultDiscountOverDependents = maximumDiscountOverDependentsOnGroup - 50;
                return resultDiscountOverDependents<= 0 ? 0 : resultDiscountOverDependents;

            case 2:
                resultDiscountOverDependents = maximumDiscountOverDependentsOnGroup - 75;
                return resultDiscountOverDependents<= 0 ? 0 : resultDiscountOverDependents;

            case 3:
                resultDiscountOverDependents = maximumDiscountOverDependentsOnGroup - 100;
                return resultDiscountOverDependents<= 0 ? 0 : resultDiscountOverDependents;


            default:
                resultDiscountOverDependents = maximumDiscountOverDependentsOnGroup - 150;
                return resultDiscountOverDependents<= 0 ? 0 : resultDiscountOverDependents;
        }

    }
}
