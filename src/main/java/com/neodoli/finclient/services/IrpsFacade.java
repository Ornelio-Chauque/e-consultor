package com.neodoli.finclient.services;

import com.neodoli.finclient.domain.DiscountPremise;
import com.neodoli.finclient.domain.DiscountPremise;
import org.springframework.stereotype.Service;

@Service
public class IrpsFacade {
    private double discountCoefficient;
    private double minimumAmountAtGroup;
    private double discountOverDependent;
    private double rawSalary;

    public double calculate(double rawSalary, int dependentNumber) {
        DiscountPremise discountPremise = new DiscountPremise(rawSalary, dependentNumber);
        this.rawSalary = rawSalary;
        discountCoefficient = new IrpsGroupCoefficentDiscountConfigure().configure(discountPremise);
        minimumAmountAtGroup = new IrpsGroupMinimumAmount().configure(discountPremise);
        discountOverDependent = new IrpsGroupDisccountOverDependents().configure(discountPremise);

        return calculateIrps();
    }

    private double calculateIrps() {
        return (rawSalary - minimumAmountAtGroup) * discountCoefficient + discountOverDependent;
    }
}
