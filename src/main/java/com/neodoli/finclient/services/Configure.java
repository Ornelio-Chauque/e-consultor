package com.neodoli.finclient.services;

import com.neodoli.finclient.domain.DiscountPremise;

import javax.servlet.Filter;

 interface IRPSConfigure {

    public double configure(DiscountPremise premise);

}
