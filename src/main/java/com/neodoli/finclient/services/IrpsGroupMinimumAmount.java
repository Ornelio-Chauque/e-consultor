package com.neodoli.finclient.services;

import com.neodoli.finclient.domain.DiscountPremise;

public class IrpsGroupMinimumAmount implements IRPSConfigure{

    @Override
    public double configure(DiscountPremise premise) {
        if(premise.getBaseSalary() <=20249.99){
            return 20249.99;
        }else if( premise.getBaseSalary()>=20250.00 && premise.getBaseSalary()<=20749.99){
            return 20250.00;
        }else if(premise.getBaseSalary()>=20750 && premise.getBaseSalary()<=20999.99){
            return 20750.0;
        }else if(premise.getBaseSalary()>=21000 && premise.getBaseSalary()<=21249.99){
            return 21000.0;
        }else if(premise.getBaseSalary()>=21250 && premise.getBaseSalary()<=21749.99){
            return 21250.0;
        }else if(premise.getBaseSalary()>=21750 && premise.getBaseSalary()<=22249.99){
            return 21750.0;
        }else if(premise.getBaseSalary()>=22250 && premise.getBaseSalary()<=32749.99){
            return 22250.0;
        }else if(premise.getBaseSalary()>=32750 && premise.getBaseSalary()<=60749.99){
            return 32750.0;
        }else if(premise.getBaseSalary()>=60750 && premise.getBaseSalary()<=144749.99){
            return 21250.0;
        }else if(premise.getBaseSalary()>=144750){
            return 144750.0;
        }
        throw new IllegalArgumentException(" The data provided are invalid");

    }
}
