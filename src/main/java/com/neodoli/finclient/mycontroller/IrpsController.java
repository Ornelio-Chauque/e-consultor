package com.neodoli.finclient.mycontroller;

import com.neodoli.finclient.services.IrpsFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/irps/")
public class IrpsController {
    @Autowired
    IrpsFacade irpsFacade;

    @GetMapping(value=("{rawSalary}/{dependentNumber}"), produces = MediaType.APPLICATION_JSON_VALUE)
    public String starter(@PathVariable("rawSalary") double rawSalary, @PathVariable("dependentNumber") int dependentNumber) {
        return String.valueOf(irpsFacade.calculate(rawSalary, dependentNumber));
    }

}
