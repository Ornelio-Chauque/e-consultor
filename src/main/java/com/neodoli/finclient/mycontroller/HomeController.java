package com.neodoli.finclient.mycontroller;

import com.neodoli.finclient.domain.IrpsResult;
import com.neodoli.finclient.services.IrpsFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;



@Controller
@RequestMapping("/")
@Validated
public class HomeController {
    private final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @GetMapping("/")
    public String index() {
        return "home";
    }

    @GetMapping(value = "/irps")
    public String displayIrpsForm(Model model) {
        model.addAttribute("rawSalary", 0);
        model.addAttribute("dependents", 0);
        return "displayIrpsForm";
    }

    @PostMapping(value = "/irps")
    public String processIrps(@RequestParam double rawSalary, @RequestParam int dependents, Model model) {
        double irpsResult = new IrpsFacade().calculate(rawSalary, dependents);
        logger.info("Your IRPS discount is " + irpsResult);
        model.addAttribute("irpsResult", irpsResult);
        return "redirect:/irpsResult?rawSalary=" + rawSalary + "&dependents=" + dependents;
    }

    @GetMapping(value = "/irpsResult")
    public String displayIrpsResult(@RequestParam double rawSalary, @RequestParam int dependents, Model model) {

        List<IrpsResult> irpsResults = new ArrayList<>();

        for (int i = 0; i <= 4; i++) {
            IrpsResult irpsResult = new IrpsResult();
            int myDependents = i;
            irpsResult.setRawSalary(rawSalary);
            irpsResult.setNumberDependents(myDependents);
            irpsResult.setIrpsDiscount(new IrpsFacade().calculate(rawSalary, myDependents));
            irpsResults.add(irpsResult);
        }

        logger.info("Your IRPS discount is " + irpsResults);

        model.addAttribute("irpsResults", irpsResults);
        return "displayIrpsResult";
    }
}
